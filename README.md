# init.sql
init.sql создаёт таблицы и заполняет их

# main.py
main.py делает запрос к БД и возвращает список преподавателей, которые принимали экзамен у студента с id=2

# app/Dockerfile
app/Dockerfile — базовая конфигурация приложения

# docker-compose.yml
docker-compose.yml поднимает два сервиса: приложение и сервис базы данных, регулирует их совместную работу

# .gitlab-ci.yml
.gitlab-ci.yml — конфигурация CI/CD
