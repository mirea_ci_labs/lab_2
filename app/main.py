import psycopg2

def connect_to_db():
    conn = psycopg2.connect(
        database="students",
        user="postgres",
        password="postgres",
        host="database",
        port="5432"
    )
    return conn

def get_teachers():
    conn = connect_to_db()
    cursor = conn.cursor()
    cursor.execute("""
	SELECT t.teacher_id, t.full_name
	FROM teachers t
	INNER JOIN grades g ON g.teacher_id = t.teacher_id
	WHERE g.student_id = 2;
    """)
    result = cursor.fetchall()
    
    print("| id   | Преподаватель |")
    print("---------------------")
    
    for subject, score in result:
        print("| {:<5} | {:<20} |".format(subject, score))

    
    cursor.close()
    conn.close()


if __name__ == "__main__":
    get_teachers()
