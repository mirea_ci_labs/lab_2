-- Создание таблицы предметов
CREATE TABLE subjects (
    subject_id SERIAL PRIMARY KEY,
    subject_name VARCHAR(100) NOT NULL
);

-- Создание таблицы студентов
CREATE TABLE students (
    student_id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    birth_date DATE NOT NULL
);

-- Создание таблицы преподавателей
CREATE TABLE teachers (
    teacher_id SERIAL PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL
);

-- Создание таблицы оценок
CREATE TABLE grades (
    grade_id SERIAL PRIMARY KEY,
    subject_id INTEGER NOT NULL REFERENCES subjects(subject_id),
    student_id INTEGER NOT NULL REFERENCES students(student_id),
    exam_date DATE NOT NULL,
    teacher_id INTEGER NOT NULL REFERENCES teachers(teacher_id),
    grade INTEGER NOT NULL
);

-- Заполнение таблицы предметов
INSERT INTO subjects (subject_name) VALUES
    ('Математика'),
    ('Физика'),
    ('Философия'),
    ('История'),
    ('Литература');

-- Заполнение таблицы студентов
INSERT INTO students (first_name, last_name, birth_date) VALUES
    ('Иван', 'Иванов', '2003-01-01'),
    ('Анна', 'Смирнова', '2001-02-03'),
    ('Михаил', 'Петров', '2002-05-10'),
    ('Екатерина', 'Сидорова', '2002-07-15'),
    ('Алексей', 'Козлов', '2003-09-20');

-- Заполнение таблицы преподавателей
INSERT INTO teachers (full_name) VALUES
    ('Иванов Иван Иванович'),
    ('Петрова Елена Николаевна'),
    ('Сидоров Андрей Викторович'),
    ('Козлова Марина Алексеевна'),
    ('Николаев Владимир Сергеевич');

-- Заполнение таблицы оценок
INSERT INTO grades (subject_id, student_id, exam_date, teacher_id, grade) VALUES
    (1, 1, '2023-01-10', 1, 4),
    (2, 1, '2023-02-15', 2, 5),
    (3, 1, '2023-03-20', 3, 4),
    (4, 1, '2023-04-25', 4, 3),
    (5, 1, '2023-05-30', 5, 5),
    (1, 2, '2023-01-10', 1, 3),
    (2, 2, '2023-02-15', 2, 4),
    (3, 2, '2023-03-20', 3, 5),
    (4, 2, '2023-04-25', 4, 2),
    (5, 2, '2023-05-30', 5, 4),
    (1, 3, '2022-01-10', 1, 5),
    (2, 3, '2022-02-15', 2, 4),
    (3, 3, '2022-03-20', 3, 3),
    (4, 3, '2022-04-25', 4, 5),
    (5, 3, '2022-05-30', 5, 4),
    (1, 4, '2022-01-10', 1, 4),
    (2, 4, '2022-02-15', 2, 5),
    (3, 4, '2022-03-20', 3, 4),
    (4, 4, '2022-04-25', 4, 3),
    (5, 4, '2022-05-30', 5, 5),
    (1, 5, '2022-01-10', 1, 5),
    (2, 5, '2022-02-15', 2, 3),
    (3, 5, '2022-03-20', 3, 4),
    (4, 5, '2022-04-25', 4, 4),
    (5, 5, '2022-05-30', 5, 5);
